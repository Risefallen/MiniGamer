//
//  SimonViewController.swift
//  MiniGamer
//
//  Created by Tim Hancock on 4/18/18.
//  Copyright © 2018 Tim Hancock. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class SimonViewController: UIViewController, AVAudioPlayerDelegate{
    
    //simon game buttons and labels
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet var soundButtons: [UIButton]!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    
    //audio vars
    var sound1: AVAudioPlayer!
    var sound2: AVAudioPlayer!
    var sound3: AVAudioPlayer!
    var sound4: AVAudioPlayer!
    
    //difficulty buttons
    @IBOutlet weak var normalButton: UIButton!
    var normalIsActive: Bool = true
    @IBOutlet weak var twoXButton: UIButton!
    var twoXIsActive: Bool = false
    @IBOutlet weak var expButton: UIButton!
    var expIsActive: Bool = false
    
    //change difficulty functions
    @IBAction func normalDifficulty(_ sender: Any) {
        if !normalIsActive{
            normalButton.backgroundColor = UIColor.gray
            normalIsActive = true
            twoXButton.backgroundColor = self.view.backgroundColor
            twoXIsActive = false
            expButton.backgroundColor = self.view.backgroundColor
            expIsActive = false
        }
    }
    
    @IBAction func twoXDifficulty(_ sender: Any) {
        if !twoXIsActive{
            twoXButton.backgroundColor = UIColor.gray
            twoXIsActive = true
            normalButton.backgroundColor = self.view.backgroundColor
            normalIsActive = false
            expButton.backgroundColor = self.view.backgroundColor
            expIsActive = false
        }
    }
    
    @IBAction func expDifficulty(_ sender: Any) {
        if !expIsActive{
            expButton.backgroundColor = UIColor.gray
            expIsActive = true
            normalButton.backgroundColor = self.view.backgroundColor
            normalIsActive = false
            twoXButton.backgroundColor = self.view.backgroundColor
            twoXIsActive = false
        }
    }
    let rate = 1.0
    let x0 = 1.0
    
    
    //game functionality vars
    var playlist = [Int]()
    var currentItem = 0
    var numTaps = 0
    var readyForUser = false
    
    var level = 1
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        normalButton.backgroundColor = UIColor.gray
        setupAudio()
    }
    
    //audio setup
    func setupAudio(){
        let soundFilePath = Bundle.main.path(forResource: "1", ofType: "wav")
        let soundFileURL = NSURL(fileURLWithPath: soundFilePath!)
        
        let soundFilePath2 = Bundle.main.path(forResource: "2", ofType: "wav")
        let soundFileURL2 = NSURL(fileURLWithPath: soundFilePath2!)
        
        let soundFilePath3 = Bundle.main.path(forResource: "3", ofType: "wav")
        let soundFileURL3 = NSURL(fileURLWithPath: soundFilePath3!)
        
        let soundFilePath4 = Bundle.main.path(forResource: "4", ofType: "wav")
        let soundFileURL4 = NSURL(fileURLWithPath: soundFilePath4!)
        
        do{
            try sound1 = AVAudioPlayer(contentsOf: soundFileURL as URL)
            try sound2 = AVAudioPlayer(contentsOf: soundFileURL2 as URL)
            try sound3 = AVAudioPlayer(contentsOf: soundFileURL3 as URL)
            try sound4 = AVAudioPlayer(contentsOf: soundFileURL4 as URL)
        }
        catch{
            print(error)
        }
        
        sound1.delegate = self
        sound2.delegate = self
        sound3.delegate = self
        sound4.delegate = self
        
        sound1.numberOfLoops = 0
        sound2.numberOfLoops = 0
        sound3.numberOfLoops = 0
        sound4.numberOfLoops = 0
    }
    
    
    //simon button interaction handler
    @IBAction func soundButtonPressed(_ sender: Any) {
        if readyForUser{
            let button = sender as! UIButton
            switch button.tag {
            case 1:
                sound1.play()
                checkIfCorrect(buttonPressed: 1)
                break
            case 2:
                sound2.play()
                checkIfCorrect(buttonPressed: 2)
                break
            case 3:
                sound3.play()
                checkIfCorrect(buttonPressed: 3)
                break
            case 4:
                sound4.play()
                checkIfCorrect(buttonPressed: 4)
                break
            default:
                break
            }
        }
    }
    
    
    //start new game button handler
    @IBAction func startGame(_ sender: Any) {
        levelLabel.text = "Level \(level)"
        scoreLabel.text = "Score: \(score)"
        if normalIsActive{
            let random = Int(arc4random_uniform(4)+1)
            playlist.append(random)
        }
        if twoXIsActive{
            for i in 0..<2{
                let random = Int(arc4random_uniform(4)+1)
                playlist.append(random)
            }
        }
        if expIsActive{
            let xt = Int(pow(x0*(1+rate),Double(level)))
            for i in 0..<xt{
                let random = Int(arc4random_uniform(4)+1)
                playlist.append(random)
            }
        }
        
        //let randomNum = Int(arc4random_uniform(4)+1)
        //playlist.append(randomNum)
        startGameButton.isHidden = true
        disabelDifficultyButtons()
        playNextItem()
    }
    
    //check if the user button was correct
    func checkIfCorrect(buttonPressed:Int){
        if buttonPressed == playlist[numTaps]{
            if numTaps == playlist.count - 1{
                
                //sleep(UInt32(1))
                //nextRound()
                //return
                score += 1
                scoreLabel.text = "Score: \(score)"
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
                    self.nextRound()
                }
                return
            }
            numTaps += 1
            score += 1
            scoreLabel.text = "Score: \(score)"
        }
        else {
            resetGame()
        }
    }
    
    //reset game when the user fails
    func resetGame(){
        level = 1
        score = 0
        readyForUser = false
        numTaps = 0
        currentItem = 0
        playlist = []
        levelLabel.text = "Game Over"
        startGameButton.isHidden = false
        enableDifficultyButtons()
    }
    
    //user passed, create next round
    func nextRound(){
        level += 1
        levelLabel.text = "Level \(level)"
        readyForUser = false
        numTaps = 0
        currentItem = 0
        
        if normalIsActive{
            let random = Int(arc4random_uniform(4)+1)
            playlist.append(random)
        }
        if twoXIsActive{
            for i in 0..<2{
                let random = Int(arc4random_uniform(4)+1)
                playlist.append(random)
            }
        }
        if expIsActive{
            let xt = Int(pow(x0*(1+rate),Double(level)))
            for i in 0..<xt{
                let random = Int(arc4random_uniform(4)+1)
                playlist.append(random)
            }
        }
        
        //let random = Int(arc4random_uniform(4)+1)
        //playlist.append(random)
        disableButtons()
        playNextItem()
    }
    
    //allows audio to finish and not overlap
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if currentItem <= playlist.count - 1{
            playNextItem()
        }
        else{
            readyForUser = true
            resetButtonHighlights()
            enableButtons()
        }
    }
    
    //plays the next item in the simon array
    func playNextItem(){
        let selectedItem = playlist[currentItem]
        
        switch selectedItem{
        case 1:
            highlightButtonWithTag(tag: 1)
            sound1.play()
            break
        case 2:
            highlightButtonWithTag(tag: 2)
            sound2.play()
            break
        case 3:
            highlightButtonWithTag(tag: 3)
            sound3.play()
            break
        case 4:
            highlightButtonWithTag(tag: 4)
            sound4.play()
            break
        default:
            break
        }
        
        currentItem += 1
    }
    
    //highlights the simon buttons
    func highlightButtonWithTag(tag:Int){
        switch tag{
        case 1:
            resetButtonHighlights()
            soundButtons[tag - 1].setImage(UIImage(named: "redPressed"), for: .normal)
            break
        case 2:
            resetButtonHighlights()
            soundButtons[tag - 1].setImage(UIImage(named: "yellowPressed"), for: .normal)
            break
        case 3:
            resetButtonHighlights()
            soundButtons[tag - 1].setImage(UIImage(named: "bluePressed"), for: .normal)
            break
        case 4:
            resetButtonHighlights()
            soundButtons[tag - 1].setImage(UIImage(named: "greenPressed"), for: .normal)
            break
        default:
            break
        }
    }
    
    func resetButtonHighlights(){
        soundButtons[0].setImage(UIImage(named:"red"), for: .normal)
        soundButtons[1].setImage(UIImage(named:"yellow"), for: .normal)
        soundButtons[2].setImage(UIImage(named:"blue"), for: .normal)
        soundButtons[3].setImage(UIImage(named:"green"), for: .normal)
    }
    
    func disableButtons(){
        for button in soundButtons{
            button.isUserInteractionEnabled = false
        }
    }
    
    func enableButtons(){
        for button in soundButtons{
            button.isUserInteractionEnabled = true
        }
    }
    
    func disabelDifficultyButtons(){
        normalButton.isUserInteractionEnabled = false
        twoXButton.isUserInteractionEnabled = false
        expButton.isUserInteractionEnabled = false
    }
    
    func enableDifficultyButtons(){
        normalButton.isUserInteractionEnabled = true
        twoXButton.isUserInteractionEnabled = true
        expButton.isUserInteractionEnabled = true
    }
    
    
}
