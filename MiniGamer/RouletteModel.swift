//
//  RouletteModel.swift
//  MiniGamer
//
//  Created by Tim Hancock on 2/16/18.
//  Copyright © 2018 Tim Hancock. All rights reserved.
//

import Foundation


class Roulette
{
    var credits: Int = 5
    
    let rouletteBoardVals: [Int] = [45,-50,25,-10,5,-25,35,-30,10,-45,30,-5,50,-20,15,-15]
    
    var angle = 0.0;
    
    func calculateValue(angle: Double)
    {
        //sleep(500)
        let moddedAngle = angle.truncatingRemainder(dividingBy: 360.00)
        let sectionOfBoard = Int(moddedAngle/22.5)
        
        credits += rouletteBoardVals[sectionOfBoard];
    }
    
}
