//
//  MemoryGame4ViewController.swift
//  MiniGamer
//
//  Created by SULTAN S2 on 4/25/18.
//  Copyright © 2018 Tim Hancock. All rights reserved.
//

import UIKit

class MemoryGame4ViewController: UIViewController {

    var fistTile   : Mylabel!
    var secondTile : Mylabel!
    var cellWidth: CGFloat!
    var cellsArr: NSMutableArray = []
    var centerArr: NSMutableArray = []
    var curTime: Int = 0
    var gameTimer: Timer = Timer()
    var compareState : Bool = false
    
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var gameView: UIView!
    
    @IBAction func resetAction(_ sender: Any) {
        for anyView in gameView.subviews
        {
            anyView.removeFromSuperview()
        }
        cellsArr = []
        centerArr = []
        blockMakerAction()
        randomAction()
        
        for anyTile in cellsArr
        {
            (anyTile as! Mylabel).text = "❓"
        }
        
        curTime = 0
        gameTimer.invalidate()
        gameTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerFunction), userInfo: nil, repeats: true)
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.resetAction(Any.self)
    }
    
    func blockMakerAction ()
    {
        cellWidth = gameView.frame.size.width / 4
        
        var xCenter: CGFloat = cellWidth / 2
        var yCenter: CGFloat = cellWidth / 2
        let cellFrame: CGRect = CGRect(x: 0.0, y: 0.0, width: cellWidth - 2 , height: cellWidth - 2)
        var counter: Int = 1
        
        
        for _ in 0..<4
        {
            for _ in 0..<4
            {
                let cell: Mylabel = Mylabel(frame: cellFrame)
                if ( counter > 8 )
                {
                    counter = 1
                }
                let cen: CGPoint = CGPoint(x: xCenter, y: yCenter)
                cell.text = String( counter )
                cell.tagNumber = counter
                cell.textAlignment = NSTextAlignment.center
                cell.font = UIFont.boldSystemFont(ofSize: 40)
                cell.textColor = UIColor.white
                cell.isUserInteractionEnabled = true
                cell.center = cen
                cell.backgroundColor = UIColor.white
                gameView.addSubview(cell)
                cellsArr.add(cell)
                centerArr.add(cen)
                xCenter = xCenter + cellWidth
                counter += 1
            }
            yCenter = yCenter + cellWidth
            xCenter = cellWidth / 2
        }
        
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let myTouch: UITouch = touches.first!
        
        if(cellsArr.contains(myTouch.view as Any))
        {
            let thisTile : Mylabel = myTouch.view as! Mylabel
            
            UIView.transition(with: thisTile, duration: 0.50, options: UIViewAnimationOptions.transitionFlipFromRight, animations: {
                thisTile.text = String(thisTile.tagNumber)
                thisTile.backgroundColor = UIColor.red
            }, completion: { (true) in
                if (self.compareState)
                {
                    self.compareState = false
                    self.secondTile = thisTile
                    self.compareAction()
                }

                else
                {
                    self.fistTile = thisTile
                    self.compareState = true
                }
                
            })
        }
        
    }
    
    // random
    func randomAction(){
        for tile in cellsArr{
            let randIndex: Int = Int(arc4random()) % centerArr.count
            let randCen: CGPoint = centerArr[randIndex] as! CGPoint
            (tile as! Mylabel).center = randCen
            centerArr.removeObject(at: randIndex)
        }
    }
    
    // function compare to cell
    func compareAction(){
        if (fistTile.tagNumber == secondTile.tagNumber)
        {
            fistTile.text = "😀"
            secondTile.text = "😀"
            fistTile.backgroundColor = UIColor.white
            secondTile.backgroundColor = UIColor.white
        }
        else
        {
            UIView.transition(with: self.view, duration: 0.50, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                self.fistTile.text = "❓"
                self.secondTile.text = "❓"
                self.fistTile.backgroundColor = UIColor.white
                self.secondTile.backgroundColor = UIColor.white
            }, completion: nil)
        }
    }
    
    
    // function timer
    @objc func timerFunction()
    {
        curTime += 1
        let timeMins: Int = curTime / 60   // (110 / 60) = 1
        let timeSecs: Int = curTime % 60  //  (110 % 60) = 50
        let timeStr : String = NSString(format: "%02d : %02d", timeMins, timeSecs) as String
        timerLabel.text = timeStr
        
    }
    
}


//sub Class
class Mylabel: UILabel {
    var tagNumber: Int!
}

