//
//  Connect4Model.swift
//  Connect4
//
//  Created by Cashe Collins on 2/9/18.
//  Copyright © 2018 Cashe Collins. All rights reserved.
//

import Foundation

class Connect4M {
    var redTurn = 2;
    var columns: [[Int]] = [[0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0]]
    
    func colorColumn(colId: Int) -> Int{
        var element = 0
        print(columns[colId][3], colId)
        for i in 0...columns[colId].count-1{
            
            element = columns[colId][i]
            if (i==0 && element==1){
                //the column is full
                //send error message saying cannot play here....do nothing
                print("serios problem man......this column is full")
            } else if (i==columns[colId].count-1 && element==0) {
                //the column is empty
                //change this spot to a one, filling that space
                columns[colId][i] = redTurn
                //return this spot to be changed
                print("The column was empty so we colored in the last one", i)
                return i
            } else if element == 0{
                //doNothing move on to the next square
            } else if element > 0 {
                //we have reached the lowest possible play space
                
                //change the spot before to a 1, filling that space
                columns[colId][i-1] = redTurn
                print("colored the " + String(i) + "th button")
                //return the spot before to color it
                return i-1
            } else {
                //something went wrong
                print("something went wrong, we should never get here")
            }
        }
        print("we made it to the end.....so something is wrong")
        return 0
    }
    
    func checkWin(colId: Int, play: Int) -> Bool{
        if vertical() {
            print("Vertical Win")
            return true;
        } else if horizontal() {
            print("Horizontal Win")
            return true;
        } else {
            return false;
        }
    }
    
    
    func vertical() -> Bool{
        for i in 0...columns.count-1{
            for j in 0...columns[i].count-4{
                if columns[i][j] == redTurn && columns[i][j+1] == redTurn && columns[i][j+2] == redTurn && columns[i][j+3] == redTurn {
                    return true;
                }
            }
        }
        return false;
    }
    
    func horizontal() -> Bool{
        for i in 0...columns.count-4{
            for j in 0...columns[i].count-1{
                if columns[i][j] == redTurn && columns[i+1][j] == redTurn && columns[i+2][j] == redTurn && columns[i+3][j] == redTurn {
                    return true;
                }
            }
        }
        return false;
    }
    
    func diagonal1() -> Bool{
        for i in 3...columns.count-4{
            for j in 3...columns[i].count-1{
                if columns[i][j] == redTurn && columns[i+1][j+1] == redTurn && columns[i+2][j+2] == redTurn && columns[i+3][j] == redTurn {
                    return true;
                }
            }
        }
        return false;
    }
    
    
    func diagonal2() -> Bool{
        for i in 3...columns.count-4{
            for j in 3...columns[i].count-1{
                if columns[i][j] == redTurn && columns[i+1][j+1] == redTurn && columns[i+2][j+2] == redTurn && columns[i+3][j] == redTurn {
                    return true;
                }
            }
        }
        return false;
    }
    
    func restart(){
        redTurn = 2;
        columns = [[0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0],
                   [0,0,0,0,0,0,0]]
        
    }
    
    
    func getBoard() -> [[Int]]{
        return columns;
    }
}
