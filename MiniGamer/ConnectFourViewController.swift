//
//  ViewController.swift
//  Connect4
//
//  Created by Cashe Collins on 2/9/18.
//  Copyright © 2018 Cashe Collins. All rights reserved.
//

import UIKit

class ConnectFourViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    var redTurn = true;
    let c4 = Connect4M()
    @IBAction func columnClick(_ sender: UIButton) {
        let column = sender.superview
        let n = Int(c4.colorColumn(colId: (column?.tag)!))
        print(column!.tag, n)
        for subview in (column?.subviews)!{
            if subview.tag == n{
                if redTurn{
                    subview.backgroundColor = UIColor.red
                } else {
                    subview.backgroundColor = UIColor.black
                }
            }
        }
        
        if c4.checkWin(colId: (column?.tag)!, play: n) {
            print("WINNER WINNER CHICKEN DINNER")
            print(c4.columns)
            var win = ""
            if redTurn == true {
                win = "RED"
            } else {
                win = "BLACK"
            }
            win = "CONGRATULATIONS " + win + " YOU WON"
            // Winner winner Prompt
            let alert = UIAlertController(title: "WINNER WINNER CHICKEN DINNER", message: win, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Return to Menu", style: UIAlertActionStyle.default, handler: { action in
                
                //Return to Main Menu
                
            }))
            alert.addAction(UIAlertAction(title: "Restart", style: UIAlertActionStyle.destructive, handler: { action in
                
                self.c4.restart();
                self.redTurn = true;
                //clear button colors
                print("RESTART WAS PRESSED!!!")
                for v in self.view.subviews{
                    for c in v.subviews{
                        for b in c.subviews{
                            b.backgroundColor = UIColor.clear;
                        }
                    }
                }
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        } else {
            //change turn
            if redTurn {
                c4.redTurn = 1;
            } else {
                c4.redTurn = 2;
            }
            redTurn = !redTurn
        }
        
    }
    
    @IBAction func rageQuit(_ sender: UIButton) {
        self.c4.restart();
        self.redTurn = true;
        //clear button colors
        print("SOMEBODY RAGE QUIT!!!")
        for v in self.view.subviews{
            for c in v.subviews{
                for b in c.subviews{
                    b.backgroundColor = UIColor.clear;
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


