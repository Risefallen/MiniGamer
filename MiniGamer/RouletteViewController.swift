//
//  RouletteViewController.swift
//  MiniGamer
//
//  Created by Tim Hancock on 2/16/18.
//  Copyright © 2018 Tim Hancock. All rights reserved.
//

import UIKit

class RouletteViewController: UIViewController {

    var mainRoulette = Roulette()
    
    @IBOutlet weak var rouletteImage: UIImageView!
    @IBOutlet weak var spinButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var creditLabel: UILabel!
    
    
    /* -- dont think i need this
     @IBAction func GoToMenu(_ sender: UIButton) {
        performSegue(withIdentifier: "MainMenuSegue", sender: self)
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resetButton.isEnabled = false
        updateCredits()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func rotateImage(_ sender: Any)
    {
        spinButton.isEnabled = false
        for i in 1...20
        {
            mainRoulette.angle += Double(arc4random_uniform(180))
            UIView.animate(withDuration: 0.5 * Double(i), animations:
                {
                self.rouletteImage.transform = CGAffineTransform(rotationAngle: CGFloat((self.mainRoulette.angle * .pi) / 180.0))
                }, completion: {(finished: Bool) in
                    if i == 20
                    {
                        self.mainRoulette.calculateValue(angle: self.mainRoulette.angle + 90)
                        self.spinButton.isEnabled = true
                        self.updateCredits()
                    }
            })
        }
    }
    
    @IBAction func resetPressed(_ sender: Any)
    {
        mainRoulette.credits = 100
        updateCredits()
        spinButton.setTitle("Spin", for: .normal)
        spinButton.setTitleColor(UIColor.white, for: .normal)
        spinButton.backgroundColor = UIColor.black
        spinButton.isEnabled = true
        resetButton.isEnabled = false
    }
    
    func updateCredits()
    {
        if mainRoulette.credits <= 0
        {
            mainRoulette.credits = 0
            spinButton.setTitle("FAIL! 🤮", for: .normal)
            spinButton.setTitleColor(UIColor.blue, for: .normal)
            spinButton.backgroundColor = UIColor.red
            
            spinButton.isEnabled = false
            resetButton.isEnabled = true
            
        }
        creditLabel.text = "Credits: \(mainRoulette.credits)"
    }
    
    
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
