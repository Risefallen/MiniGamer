//
//  MemoryGameViewController.swift
//  MiniGamer
//
//  Created by Tim Hancock on 2/16/18.
//  Copyright © 2018 Tim Hancock. All rights reserved.
//

import UIKit

class MemoryGameViewController: UIViewController {

    
    @IBAction func GoToMenu(_ sender: UIButton) {
       performSegue(withIdentifier: "MainMenuSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
